import React, { Component } from 'react'

import like from '../like.svg'
import './Tweet.css'
import api from '../services/api';

class Tweet extends Component {

    handleLike = async (e) => {
        const { tweet } = this.props

        await api.post('likes/' + tweet._id)
    }

    render() {
        const { tweet } = this.props

        return (
            <li className="tweet">
                <b>{tweet.author}</b>
                <p>{tweet.content}</p>

                <button typer="button" onClick={(e) => this.handleLike(e)}>
                    <img src={like} alt="Like" width={10} />
                    {tweet.likes}
                </button>
            </li>
        )
    }
}

export default Tweet