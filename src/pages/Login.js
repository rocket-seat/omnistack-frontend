import React, { Component } from 'react'

import twitterLogo from '../twitter.svg'
import './Login.css'

class Login extends Component {

    state = {
        username: ''
    }

    handleSubmit = (e) => {
        e.preventDefault();

        const { username } = this.state

        if (!username.length) {
            return
        }

        localStorage.setItem("@GoTwitter:username", username)

        this.props.history.push('/timeline');
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className="login-wrapper">
                <img src={twitterLogo} alt="GoTwitter" />

                <form>
                    <input type="text" placeholder="Nome do usuário" value={this.state.username} onChange={(e) => this.handleInput(e)} name="username" />
                    <button type="submit" onClick={(e) => this.handleSubmit(e)}>Entrar</button>
                </form>
            </div>
        )
    }
}

export default Login