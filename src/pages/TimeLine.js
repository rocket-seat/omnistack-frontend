import React, { Component } from 'react'
import api from '../services/api'
import Tweet from '../components/Tweet'

import socket from 'socket.io-client'

import twitterLogo from '../twitter.svg'
import './TimeLine.css'

export default class TimeLine extends Component {

    constructor(props) {
        super(props)

        if (!localStorage.getItem("@GoTwitter:username")) {
            this.props.history.push('/');
        }
    }

    async componentDidMount() {
        this.subscribeToEvents();

        const res = await api.get('tweets')
        this.setState({ tweets: res.data })
    }

    subscribeToEvents = () => {
        const io = socket('http://localhost:3000')

        io.on('novoTweet', data => {
            this.setState({
                tweets: [data, ...this.state.tweets]
            })
        })

        io.on('novoLike', data => {
            this.setState({ tweets: this.state.tweets.map(t => t._id == data._id ? data : t) })

            // Envia algo ao servidor
            // io.broadcast.emit('message', message);
            // io.emit('channel1', 'Hi server');
        })
    }

    state = {
        newTweet: '',
        tweets: []
    }

    handleInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleEnter = async (e) => {
        if (e.keyCode !== 13) {
            return
        }

        const content = this.state.newTweet

        if (!content.length) {
            return
        }

        const author = localStorage.getItem("@GoTwitter:username")

        await api.post('tweets', { content, author })
        this.setState({ newTweet: '' })
    }

    render() {
        return (
            <div className="timeline-wrapper">
                <img height={50} width={50} src={twitterLogo} alt="GoTwitter" />

                <form>
                    <textarea name="newTweet" placeholder="O que está acontecendo?" value={this.state.newTweet} onChange={(e) => this.handleInput(e)} onKeyDown={(e) => this.handleEnter(e)}></textarea>
                </form>

                <ul className="tweet-list">
                    {this.state.tweets.map((t, i) => (
                        <Tweet tweet={t} key={i} />
                    ))}
                </ul>
            </div>
        )
    }
}
